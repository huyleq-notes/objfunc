import matplotlib 
import matplotlib.pyplot as plt
from scipy.signal import hilbert

import sys
import numpy as np
import math

from seppyio import *

pi=math.pi
epsilon=sys.float_info.min
plt.rcParams.update({'font.size':15})

def l2(w,w0):
    r=w-w0
    return np.sum(r*r) 

def h(w,a):
    a2=a*a
    w2=w*w
    nw=w.shape[0]
    nahalf=a.shape[0]
    na=2*nahalf+1
    h=np.zeros(nw)
    for i in range(nw):
        s=w2[i]
        for j in range(nahalf):
            k=j+1
            
            # 0 boundary
#            ik=i+k
#            if ik<nw:
#                s=s+a2[j]*w2[ik]
#            ik=i-k
#            if ik>=0:
#                s=s+a2[j]*w2[ik]
            
            # periodic boundary
            ik=i+k
            if ik>=nw:
                ik=ik-nw
            s=s+a2[j]*w2[ik]
            ik=i-k
            if ik<0:
                ik=ik+nw
            s=s+a2[j]*w2[ik]

        s=max(epsilon,s)
        h[i]=w[i]/math.sqrt(s)
    return h

def ricker(n,dt,freq,tdelay):
    m=2*n+1
    ricker=np.zeros(m)
    t=np.zeros(m)
    pi2f2=pi*pi*freq*freq
    for i in range(m):
        j=i-n
        t[i]=j*dt
        tt=t[i]-tdelay
        pi2f2t2=pi2f2*tt*tt
        ricker[i]=(1.-2.*pi2f2t2)*math.exp(-pi2f2t2)
    ricker=ricker/np.amax(ricker)
    return ricker,t

def ricker1st(n,dt,freq,tdelay):
    m=2*n+1
    ricker1st=np.zeros(m)
    t=np.zeros(m)
    pi2f2=pi*pi*freq*freq
    for i in range(m):
        j=i-n
        t[i]=j*dt
        tt=t[i]-tdelay
        pi2f2t2=pi2f2*tt*tt
        ricker1st[i]=-2.*pi2f2*tt*(3.-2.*pi2f2t2)*math.exp(-pi2f2t2)
    ricker1st=ricker1st/np.amax(ricker1st)
    return ricker1st,t

def normalcoeff(winlen,dt):
    nah=int(winlen/2./dt)
    a=np.zeros(nah)
    sigma=winlen/6.
    sigma2=2*sigma*sigma
    for i in range(nah):
        j=i+1
        t=j*dt
        a[i]=math.exp(-t*t/sigma2)
    return a

def envelope(w):
    return np.abs(hilbert(w))

freq10=10.
freq5=5.
dt=0.004
n=120
m=2*n+1

ricker1st10,t=ricker1st(n,dt,freq10,0.)
ricker10,t=ricker(n,dt,freq10,0.)
ricker1st5,t=ricker1st(n,dt,freq5,0.)
ricker5,t=ricker(n,dt,freq5,0.)

ds=0.004
d=int(ds/dt)
k=2*n/3/d
nlag=2*k+1
objfunc=np.zeros(nlag)
tau=np.zeros(nlag)

test=int(get_param("test"))
func=int(get_param("func"))
scalefactor=get_param("scalefactor")

surfix='func'
if func==1:
    w0=ricker5
    surfix=surfix+str(func)
elif func==2:
    w0=ricker10
    surfix=surfix+str(func)
elif func==3: 
    w0=ricker1st5
    surfix=surfix+str(func)
else:
    w0=ricker1st10
    surfix=surfix+str(func)

w0=w0*scalefactor

surfix=surfix+'scale'
if scalefactor==1e0:
    surfix=surfix+'1e0.pdf'
elif scalefactor==5e13:
    surfix=surfix+'5e13.pdf'
elif scalefactor==1e-6:
    surfix=surfix+'1e-6.pdf'
else:
    surfix=surfix+str(scalefactor)+'.pdf'

if test==0:
    fig1=plt.figure(1)
    ax1=fig1.add_subplot(111)
    ax1.plot(t,ricker5,linewidth=3)
    ax1.set_xlabel('time (s)')
    ax1.set_xlim(-0.5,0.5)
    fig1.savefig('figures/modeleddata.pdf',bbox_inches='tight')

    fig2=plt.figure(2)
    ax2=fig2.add_subplot(111)
    ax2.plot(t,w0,linewidth=3)
    ax2.set_xlabel('time (s)')
    ax2.set_xlim(-0.5,0.5)
    ax2.set_xlim(-0.5,0.5)
    name='figures/data'+surfix
    fig2.savefig(name,bbox_inches='tight')

    for i in range(nlag):
        j=(i-k)*d
        tau[i]=j*dt
        w,_=ricker(n,dt,freq5,tau[i])
        objfunc[i]=l2(w,w0)
    
    fig3=plt.figure(3)
    ax3=fig3.add_subplot(111)
    ax3.plot(tau,objfunc,linewidth=2)
    ax3.scatter(tau,objfunc,s=50)
    ax3.set_xlabel("time lag(s)")
    ax3.set_xlim(-0.4,0.4)
    name='figures/objfunctest0'+surfix
    fig3.savefig(name,bbox_inches='tight')

elif test==1:
    fig1=plt.figure(1)
    ax1=fig1.add_subplot(111)
    fig2=plt.figure(2)
    ax2=fig2.add_subplot(111)
    fig3=plt.figure(3)
    ax3=fig3.add_subplot(111)
    fig4=plt.figure(4)
    ax4=fig4.add_subplot(111)
    
    winlen=2.*dt
    colors=['b','g','r','c','m','y','k']
    count=0

    ax3.plot(t,ricker5,linewidth=2,linestyle='--')
    if scalefactor==1e0:
        ax2.plot(t,w0,linewidth=2,linestyle='--')

    while winlen<0.6:
        a=normalcoeff(winlen,dt)
        x=np.linspace(0.,winlen/2,a.shape[0]+1)
        y=np.concatenate((np.ones(1),a))
        ax1.plot(x,y,linewidth=3,c=colors[count])
        
        hw0=h(w0,a)
        ax2.plot(t,hw0,linewidth=3,c=colors[count])

        hricker5=h(ricker5,a)
        ax3.plot(t,hricker5,linewidth=3,c=colors[count])
        
        for i in range(nlag):
            j=(i-k)*d
            tau[i]=j*dt
            w,_=ricker(n,dt,freq5,tau[i])
            hw=h(w,a)
            objfunc[i]=l2(hw,hw0)
        
        ax4.plot(tau,objfunc,linewidth=2,c=colors[count])
        ax4.scatter(tau,objfunc,c=colors[count],edgecolors=colors[count],s=20)

        winlen=winlen*2
        count=count+1

    ax1.set_xlabel("time lag (s)")
    fig1.savefig('figures/a.pdf',bbox_inches='tight')

    ax2.set_xlabel("time (s)")
    ax2.set_ylim(-1.2,1.2)
    ax2.set_xlim(-0.5,0.5)
    name='figures/phdata'+surfix
    fig2.savefig(name,bbox_inches='tight')

    ax3.set_xlabel("time (s)")
    ax3.set_xlim(-0.5,0.5)
    name='figures/phmodeleddata.pdf'
    fig3.savefig(name,bbox_inches='tight')
    
    ax4.set_xlabel("time lag (s)")
    ax4.set_xlim(-0.4,0.4)
    name='figures/pobjfunctest1'+surfix
    fig4.savefig(name,bbox_inches='tight')

elif test==2:
    ericker5=envelope(ricker5)
    fig1=plt.figure(1)
    ax1=fig1.add_subplot(111)
    ax1.plot(t,ricker5,linewidth=3)
    ax1.plot(t,ericker5,linewidth=3)
    ax1.set_xlabel('time (s)')
    ax1.set_xlim(-0.5,0.5)
    fig1.savefig('figures/emodeleddata.pdf',bbox_inches='tight')

    ew0=envelope(w0)
    fig2=plt.figure(2)
    ax2=fig2.add_subplot(111)
    ax2.plot(t,w0,linewidth=3)
    ax2.plot(t,ew0,linewidth=3)
    ax2.set_xlabel('time (s)')
    ax2.set_xlim(-0.5,0.5)
    ax2.set_xlim(-0.5,0.5)
    name='figures/edata'+surfix
    fig2.savefig(name,bbox_inches='tight')

    for i in range(nlag):
        j=(i-k)*d
        tau[i]=j*dt
        w,_=ricker(n,dt,freq5,tau[i])
        ew=envelope(w)
        objfunc[i]=l2(ew,ew0)
    
    fig3=plt.figure(3)
    ax3=fig3.add_subplot(111)
    ax3.plot(tau,objfunc,linewidth=2)
    ax3.scatter(tau,objfunc,s=50)
    ax3.set_xlabel("time lag(s)")
    ax3.set_xlim(-0.4,0.4)
    name='figures/objfunctest2'+surfix
    fig3.savefig(name,bbox_inches='tight')

elif test==3:
    fig2=plt.figure(2)
    ax2=fig2.add_subplot(111)
    fig3=plt.figure(3)
    ax3=fig3.add_subplot(111)
    fig4=plt.figure(4)
    ax4=fig4.add_subplot(111)
    
    winlen=2.*dt
    colors=['b','g','r','c','m','y','k']
    count=0

    ax3.plot(t,ricker5,linewidth=2,linestyle=':')
    if scalefactor==1e0:
        ax2.plot(t,w0,linewidth=2,linestyle=':')

    while winlen<0.6:
        a=normalcoeff(winlen,dt)
        hw0=h(w0,a)
        ehw0=envelope(hw0)
        ax2.plot(t,hw0,linewidth=2,linestyle='--',c=colors[count])
        ax2.plot(t,ehw0,linewidth=3,c=colors[count])

        hricker5=h(ricker5,a)
        ehricker5=envelope(hricker5)
        ax3.plot(t,hricker5,linewidth=2,linestyle='--',c=colors[count])
        ax3.plot(t,ehricker5,linewidth=3,c=colors[count])
        
        for i in range(nlag):
            j=(i-k)*d
            tau[i]=j*dt
            w,_=ricker(n,dt,freq5,tau[i])
            hw=h(w,a)
            ehw=envelope(hw)
            objfunc[i]=l2(ehw,ehw0)
        
        ax4.plot(tau,objfunc,linewidth=2,c=colors[count])
        ax4.scatter(tau,objfunc,c=colors[count],edgecolors=colors[count],s=20)

        winlen=winlen*2
        count=count+1

    ax2.set_xlabel("time (s)")
    ax2.set_xlim(-0.5,0.5)
    name='figures/pehdata'+surfix
    fig2.savefig(name,bbox_inches='tight')

    ax3.set_xlabel("time (s)")
    ax3.set_xlim(-0.5,0.5)
    name='figures/pehmodeleddata.pdf'
    fig3.savefig(name,bbox_inches='tight')
    
    ax4.set_xlabel("time lag (s)")
    ax4.set_xlim(-0.4,0.4)
    name='figures/pobjfunctest3'+surfix
    fig4.savefig(name,bbox_inches='tight')

elif test==4:
    fig2=plt.figure(2)
    ax2=fig2.add_subplot(111)
    fig3=plt.figure(3)
    ax3=fig3.add_subplot(111)
    fig4=plt.figure(4)
    ax4=fig4.add_subplot(111)
    
    winlen=2.*dt
    colors=['b','g','r','c','m','y','k']
    count=0

    ew0=envelope(w0)
    ericker5=envelope(ricker5)
    ax3.plot(t,ricker5,linewidth=2,linestyle=':',c='b')
    ax3.plot(t,ericker5,linewidth=2,linestyle='--',c='b')
    if scalefactor==1e0:
        ax2.plot(t,w0,linewidth=2,linestyle=':',c='b')
        ax2.plot(t,ew0,linewidth=2,linestyle='--',c='b')

    while winlen<0.6:
        a=normalcoeff(winlen,dt)
        hew0=h(ew0,a)
        ax2.plot(t,hew0,linewidth=3,c=colors[count])

        hericker5=h(ericker5,a)
        ax3.plot(t,hericker5,linewidth=3,c=colors[count])
        
        for i in range(nlag):
            j=(i-k)*d
            tau[i]=j*dt
            w,_=ricker(n,dt,freq5,tau[i])
            ew=envelope(w)
            hew=h(ew,a)
            objfunc[i]=l2(hew,hew0)
        
        ax4.plot(tau,objfunc,linewidth=2,c=colors[count])
        ax4.scatter(tau,objfunc,c=colors[count],edgecolors=colors[count],s=20)

        winlen=winlen*2
        count=count+1

    ax2.set_xlabel("time (s)")
    ax2.set_xlim(-0.5,0.5)
    name='figures/phedata'+surfix
    fig2.savefig(name,bbox_inches='tight')

    ax3.set_xlabel("time (s)")
    ax3.set_xlim(-0.5,0.5)
    name='figures/phemodeleddata.pdf'
    fig3.savefig(name,bbox_inches='tight')
    
    ax4.set_xlabel("time lag (s)")
    ax4.set_xlim(-0.4,0.4)
    name='figures/pobjfunctest4'+surfix
    fig4.savefig(name,bbox_inches='tight')

elif test==5:
    fig2=plt.figure(2)
    ax2=fig2.add_subplot(111)
    fig3=plt.figure(3)
    ax3=fig3.add_subplot(111)
    fig4=plt.figure(4)
    ax4=fig4.add_subplot(111)
    
    winlen=2.*dt
    colors=['b','g','r','c','m','y','k']
    count=0

    ax3.plot(t,ricker5,linewidth=2,linestyle='--')
    if scalefactor==1e0:
        ax2.plot(t,w0,linewidth=2,linestyle='--')

    while winlen<0.6:
        a=np.ones(int(winlen/dt))
        hw0=h(w0,a)
        ax2.plot(t,hw0,linewidth=3,c=colors[count])

        hricker5=h(ricker5,a)
        ax3.plot(t,hricker5,linewidth=3,c=colors[count])
        
        for i in range(nlag):
            j=(i-k)*d
            tau[i]=j*dt
            w,_=ricker(n,dt,freq5,tau[i])
            hw=h(w,a)
            objfunc[i]=l2(hw,hw0)
        
        ax4.plot(tau,objfunc,linewidth=2,c=colors[count])
        ax4.scatter(tau,objfunc,c=colors[count],edgecolors=colors[count],s=20)

        winlen=winlen*2
        count=count+1

    ax2.set_xlabel("time (s)")
    #ax2.set_ylim(-1.2,1.2)
    ax2.set_xlim(-0.5,0.5)
    name='figures/padata'+surfix
    fig2.savefig(name,bbox_inches='tight')

    ax3.set_xlabel("time (s)")
    ax3.set_xlim(-0.5,0.5)
    name='figures/pamodeleddata.pdf'
    fig3.savefig(name,bbox_inches='tight')
    
    ax4.set_xlabel("time lag (s)")
    ax4.set_xlim(-0.4,0.4)
    name='figures/pobjfunctest5'+surfix
    fig4.savefig(name,bbox_inches='tight')

elif test==6:
    fig2=plt.figure(2)
    ax2=fig2.add_subplot(111)
    fig3=plt.figure(3)
    ax3=fig3.add_subplot(111)
    fig4=plt.figure(4)
    ax4=fig4.add_subplot(111)
    
    winlen=2.*dt
    colors=['b','g','r','c','m','y','k']
    count=0

    ax3.plot(t,ricker5,linewidth=2,linestyle=':')
    if scalefactor==1e0:
        ax2.plot(t,w0,linewidth=2,linestyle=':')

    while winlen<0.6:
        a=np.ones(int(winlen/dt))
        hw0=h(w0,a)
        ehw0=envelope(hw0)
        ax2.plot(t,hw0,linewidth=2,linestyle='--',c=colors[count])
        ax2.plot(t,ehw0,linewidth=3,c=colors[count])

        hricker5=h(ricker5,a)
        ehricker5=envelope(hricker5)
        ax3.plot(t,hricker5,linewidth=2,linestyle='--',c=colors[count])
        ax3.plot(t,ehricker5,linewidth=3,c=colors[count])
        
        for i in range(nlag):
            j=(i-k)*d
            tau[i]=j*dt
            w,_=ricker(n,dt,freq5,tau[i])
            hw=h(w,a)
            ehw=envelope(hw)
            objfunc[i]=l2(ehw,ehw0)
        
        ax4.plot(tau,objfunc,linewidth=2,c=colors[count])
        ax4.scatter(tau,objfunc,c=colors[count],edgecolors=colors[count],s=20)

        winlen=winlen*2
        count=count+1

    ax2.set_xlabel("time (s)")
    ax2.set_xlim(-0.5,0.5)
    name='figures/peadata'+surfix
    fig2.savefig(name,bbox_inches='tight')

    ax3.set_xlabel("time (s)")
    ax3.set_xlim(-0.5,0.5)
    name='figures/peamodeleddata.pdf'
    fig3.savefig(name,bbox_inches='tight')
    
    ax4.set_xlabel("time lag (s)")
    ax4.set_xlim(-0.4,0.4)
    name='figures/pobjfunctest6'+surfix
    fig4.savefig(name,bbox_inches='tight')

elif test==7:
    fig2=plt.figure(2)
    ax2=fig2.add_subplot(111)
    fig3=plt.figure(3)
    ax3=fig3.add_subplot(111)
    fig4=plt.figure(4)
    ax4=fig4.add_subplot(111)
    
    winlen=2.*dt
    colors=['b','g','r','c','m','y','k']
    count=0

    ew0=envelope(w0)
    ericker5=envelope(ricker5)
    ax3.plot(t,ricker5,linewidth=2,linestyle=':',c='b')
    ax3.plot(t,ericker5,linewidth=2,linestyle='--',c='b')
    if scalefactor==1e0:
        ax2.plot(t,w0,linewidth=2,linestyle=':',c='b')
        ax2.plot(t,ew0,linewidth=2,linestyle='--',c='b')

    while winlen<0.6:
        a=np.ones(int(winlen/dt))
        hew0=h(ew0,a)
        ax2.plot(t,hew0,linewidth=3,c=colors[count])

        hericker5=h(ericker5,a)
        ax3.plot(t,hericker5,linewidth=3,c=colors[count])
        
        for i in range(nlag):
            j=(i-k)*d
            tau[i]=j*dt
            w,_=ricker(n,dt,freq5,tau[i])
            ew=envelope(w)
            hew=h(ew,a)
            objfunc[i]=l2(hew,hew0)
        
        ax4.plot(tau,objfunc,linewidth=2,c=colors[count])
        ax4.scatter(tau,objfunc,c=colors[count],edgecolors=colors[count],s=20)

        winlen=winlen*2
        count=count+1

    ax2.set_xlabel("time (s)")
    ax2.set_xlim(-0.5,0.5)
    name='figures/paedata'+surfix
    fig2.savefig(name,bbox_inches='tight')

    ax3.set_xlabel("time (s)")
    ax3.set_xlim(-0.5,0.5)
    name='figures/paemodeleddata.pdf'
    fig3.savefig(name,bbox_inches='tight')
    
    ax4.set_xlabel("time lag (s)")
    ax4.set_xlim(-0.4,0.4)
    name='figures/pobjfunctest7'+surfix
    fig4.savefig(name,bbox_inches='tight')

    
#plt.show()


