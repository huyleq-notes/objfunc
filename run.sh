#!/usr/bin/env tcsh

#python test-objfunc.py test=0 func=1 scalefactor=1e0   
#python test-objfunc.py test=0 func=1 scalefactor=1e-6   
#python test-objfunc.py test=0 func=1 scalefactor=5e13   
#
#python test-objfunc.py test=0 func=2 scalefactor=1e0   
#python test-objfunc.py test=0 func=2 scalefactor=1e-6   
#python test-objfunc.py test=0 func=2 scalefactor=5e13   
#
#python test-objfunc.py test=0 func=3 scalefactor=1e0   
#python test-objfunc.py test=0 func=3 scalefactor=1e-6   
#python test-objfunc.py test=0 func=3 scalefactor=5e13   
#
#python test-objfunc.py test=0 func=4 scalefactor=1e0   
#python test-objfunc.py test=0 func=4 scalefactor=1e-6   
#python test-objfunc.py test=0 func=4 scalefactor=5e13   
#
python test-objfunc.py test=1 func=1 scalefactor=1e0   
python test-objfunc.py test=1 func=1 scalefactor=1e-6   
python test-objfunc.py test=1 func=1 scalefactor=5e13   

python test-objfunc.py test=1 func=2 scalefactor=1e0   
python test-objfunc.py test=1 func=2 scalefactor=1e-6   
python test-objfunc.py test=1 func=2 scalefactor=5e13   

python test-objfunc.py test=1 func=3 scalefactor=1e0   
python test-objfunc.py test=1 func=3 scalefactor=1e-6   
python test-objfunc.py test=1 func=3 scalefactor=5e13   

python test-objfunc.py test=1 func=4 scalefactor=1e0   
python test-objfunc.py test=1 func=4 scalefactor=1e-6   
python test-objfunc.py test=1 func=4 scalefactor=5e13   
#
#python test-objfunc.py test=2 func=1 scalefactor=1e0   
#python test-objfunc.py test=2 func=1 scalefactor=1e-6   
#python test-objfunc.py test=2 func=1 scalefactor=5e13   
#
#python test-objfunc.py test=2 func=2 scalefactor=1e0   
#python test-objfunc.py test=2 func=2 scalefactor=1e-6   
#python test-objfunc.py test=2 func=2 scalefactor=5e13   
#
#python test-objfunc.py test=2 func=3 scalefactor=1e0   
#python test-objfunc.py test=2 func=3 scalefactor=1e-6   
#python test-objfunc.py test=2 func=3 scalefactor=5e13   
#
#python test-objfunc.py test=2 func=4 scalefactor=1e0   
#python test-objfunc.py test=2 func=4 scalefactor=1e-6   
#python test-objfunc.py test=2 func=4 scalefactor=5e13   

python test-objfunc.py test=3 func=1 scalefactor=1e0   
python test-objfunc.py test=3 func=1 scalefactor=1e-6   
python test-objfunc.py test=3 func=1 scalefactor=5e13   

python test-objfunc.py test=3 func=2 scalefactor=1e0   
python test-objfunc.py test=3 func=2 scalefactor=1e-6   
python test-objfunc.py test=3 func=2 scalefactor=5e13   

python test-objfunc.py test=3 func=3 scalefactor=1e0   
python test-objfunc.py test=3 func=3 scalefactor=1e-6   
python test-objfunc.py test=3 func=3 scalefactor=5e13   

python test-objfunc.py test=3 func=4 scalefactor=1e0   
python test-objfunc.py test=3 func=4 scalefactor=1e-6   
python test-objfunc.py test=3 func=4 scalefactor=5e13   

python test-objfunc.py test=4 func=1 scalefactor=1e0   
python test-objfunc.py test=4 func=1 scalefactor=1e-6   
python test-objfunc.py test=4 func=1 scalefactor=5e13   

python test-objfunc.py test=4 func=2 scalefactor=1e0   
python test-objfunc.py test=4 func=2 scalefactor=1e-6   
python test-objfunc.py test=4 func=2 scalefactor=5e13   

python test-objfunc.py test=4 func=3 scalefactor=1e0   
python test-objfunc.py test=4 func=3 scalefactor=1e-6   
python test-objfunc.py test=4 func=3 scalefactor=5e13   

python test-objfunc.py test=4 func=4 scalefactor=1e0   
python test-objfunc.py test=4 func=4 scalefactor=1e-6   
python test-objfunc.py test=4 func=4 scalefactor=5e13   

python test-objfunc.py test=5 func=1 scalefactor=1e0   
python test-objfunc.py test=5 func=1 scalefactor=1e-6   
python test-objfunc.py test=5 func=1 scalefactor=5e13   

python test-objfunc.py test=5 func=2 scalefactor=1e0   
python test-objfunc.py test=5 func=2 scalefactor=1e-6   
python test-objfunc.py test=5 func=2 scalefactor=5e13   

python test-objfunc.py test=5 func=3 scalefactor=1e0   
python test-objfunc.py test=5 func=3 scalefactor=1e-6   
python test-objfunc.py test=5 func=3 scalefactor=5e13   

python test-objfunc.py test=5 func=4 scalefactor=1e0   
python test-objfunc.py test=5 func=4 scalefactor=1e-6   
python test-objfunc.py test=5 func=4 scalefactor=5e13   

python test-objfunc.py test=6 func=1 scalefactor=1e0   
python test-objfunc.py test=6 func=1 scalefactor=1e-6   
python test-objfunc.py test=6 func=1 scalefactor=5e13   

python test-objfunc.py test=6 func=2 scalefactor=1e0   
python test-objfunc.py test=6 func=2 scalefactor=1e-6   
python test-objfunc.py test=6 func=2 scalefactor=5e13   

python test-objfunc.py test=6 func=3 scalefactor=1e0   
python test-objfunc.py test=6 func=3 scalefactor=1e-6   
python test-objfunc.py test=6 func=3 scalefactor=5e13   

python test-objfunc.py test=6 func=4 scalefactor=1e0   
python test-objfunc.py test=6 func=4 scalefactor=1e-6   
python test-objfunc.py test=6 func=4 scalefactor=5e13   

python test-objfunc.py test=7 func=1 scalefactor=1e0   
python test-objfunc.py test=7 func=1 scalefactor=1e-6   
python test-objfunc.py test=7 func=1 scalefactor=5e13   

python test-objfunc.py test=7 func=2 scalefactor=1e0   
python test-objfunc.py test=7 func=2 scalefactor=1e-6   
python test-objfunc.py test=7 func=2 scalefactor=5e13   

python test-objfunc.py test=7 func=3 scalefactor=1e0   
python test-objfunc.py test=7 func=3 scalefactor=1e-6   
python test-objfunc.py test=7 func=3 scalefactor=5e13   

python test-objfunc.py test=7 func=4 scalefactor=1e0   
python test-objfunc.py test=7 func=4 scalefactor=1e-6   
python test-objfunc.py test=7 func=4 scalefactor=5e13   
